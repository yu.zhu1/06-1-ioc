package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class WithAutowiredMethod {

    public WithAutowiredMethod(Dependent dependent, Logger logger) {
        this.dependent = dependent;
        this.logger = logger;
        this.logger.addLog("this is dependent");
    }

    public Dependent getDependent() {

        return dependent;
    }

    public AnotherDependent getAnotherDependent() {
        return anotherDependent;
    }

    private Dependent dependent;

    public Logger getLogger() {
        return logger;
    }

    private Logger logger;
    private AnotherDependent anotherDependent;

    @Autowired
    public void initialize(AnotherDependent anotherDependent) {
        logger.addLog("this is another dependent");
        this.anotherDependent = anotherDependent;
    }
}
