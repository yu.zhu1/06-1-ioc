package com.twuc.webApp.yourTurn;

public class SimpleObject implements SimpleInterface {
    private SimpleDependent simpleDependent;

    public SimpleDependent getSimpleDependent() {
        return simpleDependent;
    }

    public SimpleObject(SimpleDependent simpleDependent) {
        this.simpleDependent = simpleDependent;
    }


}
