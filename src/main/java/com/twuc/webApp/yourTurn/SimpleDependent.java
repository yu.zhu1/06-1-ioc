package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class SimpleDependent {
    public void setName(String name) {
        Name = name;
    }

    public String getName() {
        return Name;
    }



    private String Name;
}
