package com.twuc.webApp.yourTurn;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class AnnotationConfigOtherEgTest {

    private AnnotationConfigApplicationContext context;


    @BeforeEach
    public void createContext() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void should_use_the_first_constructor() {
        MultipleConstructor result = context.getBean(MultipleConstructor.class);
        assertNotNull(result.getDependent());
        assertNull(result.getName());
    }

    @Test
    void should_call_constructor_and_initialized() {
        WithAutowiredMethod result = context.getBean(WithAutowiredMethod.class);
        assertNotNull(result.getAnotherDependent());
        assertNotNull(result.getDependent());
        assertEquals("this is dependent", result.getLogger().getLogger().get(0));
        assertEquals("this is another dependent", result.getLogger().getLogger().get(1));
    }

    @Test
    void should_create_array_list_with_context() {
        Map<String, InterfaceWithMultipleImpls> result = context.getBeansOfType(InterfaceWithMultipleImpls.class);
        Object[] outcome = result.values().stream().map(
                (item) -> {
                    return item.getClass().getName();
                }
        ).sorted().toArray();
    assertEquals(3, outcome.length);
    assertSame("com.twuc.webApp.yourTurn.ImplementationA", outcome[0]);
    assertSame("com.twuc.webApp.yourTurn.ImplementationB", outcome[1]);
    assertSame("com.twuc.webApp.yourTurn.ImplementationC", outcome[2]);
    }

}
