package com.twuc.webApp.yourTurn;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import out.of.scanning.webApp.OutOfScanningScope;

import static org.junit.jupiter.api.Assertions.*;

public class AnnotationConfigTest {
    private AnnotationConfigApplicationContext context;


    @BeforeEach
    public void createContext() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void should_create_object() {
        WithoutDependency result = context.getBean(WithoutDependency.class);
        assertNotNull(result.getClass());
        assertSame(WithoutDependency.class, result.getClass());
    }

    @Test
    void should_create_object_and_dependence() {
        WithoutDependency withoutDependence = context.getBean(WithoutDependency.class);
        assertNotNull(Dependent.class);
        assertSame(Dependent.class, withoutDependence.getDependent().getClass());
    }

    @Test
    void should_create_object_out_of_scope() {
        assertThrows(Exception.class, () -> {
                OutOfScanningScope outOfScanningScope = context.getBean(OutOfScanningScope.class);
        });
    }

    @Test
    void should_create_interfaceImp() {
        Interface result = context.getBean(Interface.class);
        assertNotNull(result);
    }

    @Test
    void should_() {
        SimpleObject result = (SimpleObject)context.getBean(SimpleInterface.class);
        assertEquals("O_o", result.getSimpleDependent().getName());
    }


}
