package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {
    @Autowired
    public MultipleConstructor(Dependent dependent) {
        this.dependent = dependent;
    }
    @Autowired
    private Dependent dependent;

    public Dependent getDependent() {
        return dependent;
    }

    public MultipleConstructor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    private String name;
}
