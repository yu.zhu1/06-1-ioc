package com.twuc.webApp.yourTurn;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleObjectFactory {


    @Bean
    public SimpleObject simpleObject(SimpleDependent simpleDependent) {
        simpleDependent.setName("O_o");
        return new SimpleObject(simpleDependent);
    }

}
